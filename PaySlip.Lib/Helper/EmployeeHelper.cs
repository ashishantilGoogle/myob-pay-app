﻿using PaySlip.Lib.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySlip.Lib.Helper
{
    public static class EmployeeHelper
    {


        /// <summary>
        /// must have five fields in the line
        /// </summary>
        /// <param name="lineArray"></param>
        /// <returns></returns>
        public static bool IsValidRow(string[] lineArray)
        {
            return lineArray.Length == 5;
        }

        /// <summary>
        /// Validate Employee pay record line before start processing
        /// </summary>
        /// <param name="employeeLine"></param>
        /// <returns></returns>
        public static EmployeeDetail GetEmployeeDetail(string employeeLine)
        {
            var employeeDetail = new EmployeeDetail();
            if (string.IsNullOrEmpty(employeeLine))
            {
                employeeDetail.ErrorMessage = "Invalid row";
                return employeeDetail;
            }

            var employeeLineRow = employeeLine.Split(',');
            if (!IsValidRow(employeeLineRow))
            {
                employeeDetail.ErrorMessage = "Invalid data line";
                return employeeDetail;
            }

            employeeDetail.FirstName = employeeLineRow[0];
            if (string.IsNullOrEmpty((employeeDetail.FirstName)))
            {
                employeeDetail.ErrorMessage = "Invalid first name";
                return employeeDetail;
            }

            employeeDetail.LastName = employeeLineRow[1];
            if (string.IsNullOrEmpty((employeeDetail.LastName)))
            {
                employeeDetail.ErrorMessage = "Invalid last name";
                return employeeDetail;
            }

            int annualSalaryValue;
            int.TryParse(employeeLineRow[2], out annualSalaryValue);
            employeeDetail.AnnualSalary = annualSalaryValue;
            if (!HasValidAnnualSalary(employeeDetail.AnnualSalary))
            {
                employeeDetail.ErrorMessage = "Invalid annual salary";
                return employeeDetail;
            }

            if (!HasValidSuperRate(employeeLineRow[3], employeeDetail))
                return employeeDetail;

            employeeDetail.PaymentStartDate = employeeLineRow[4];
            if (string.IsNullOrEmpty((employeeDetail.PaymentStartDate)))
            {
                employeeDetail.ErrorMessage = "Invalid payment start date";
                return employeeDetail;
            }

            //all  good
            employeeDetail.IsValid = true;
            return employeeDetail;
        }

        /// <summary>
        /// annual salary should be (positive integer)
        /// </summary>
        /// <param name="annualSalary"></param>
        /// <returns></returns>
        private static bool HasValidAnnualSalary(int annualSalary)
        {
            return annualSalary > 0;
        }

        /// <summary>
        /// super rate (0% - 50% inclusive)
        /// </summary>
        /// <param name="superRateValue">50%</param>
        /// <returns></returns>
        private static bool HasValidSuperRate(string superRateValue, EmployeeDetail employeeDetail)
        {
            if (string.IsNullOrEmpty(superRateValue))
            {
                employeeDetail.ErrorMessage = "invalid super rate.";
                return false;
            }

            superRateValue = superRateValue.Trim().TrimEnd('%');
            double superRate;
            double.TryParse(superRateValue, out superRate);
            employeeDetail.SuperRate = superRate / 100;

            if (employeeDetail.SuperRate < 0)
            {
                //super rate cannot be less than zero
                employeeDetail.ErrorMessage = "invalid super rate.";
                return false;
            }
            return true;
        }

    }




}
