﻿using PaySlip.Lib.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySlip.Lib.Helper
{
    class TaxableIncomeRates
    {
        /// <summary>
		/// Taxable income rates
		/// The following rates for 2017-18 apply from 1 July 2017
		/// This list is hard coded at the moment, we can move this to store later
		/// </summary>
		public static List<TaxableIncome> GetRates()
        {
            return new List<TaxableIncome>() {
                new TaxableIncome(){AnnualTaxableIncome=18200, AnnualTaxFix=0, PlusRate=0.19},// 19c
				new TaxableIncome(){AnnualTaxableIncome=37000, AnnualTaxFix=3572, PlusRate=0.325}, //32.5c
				new TaxableIncome(){AnnualTaxableIncome=87000, AnnualTaxFix=19822, PlusRate=0.37},
                new TaxableIncome(){AnnualTaxableIncome=180000, AnnualTaxFix=54232, PlusRate=0.45},
            };
        }
    }
}
