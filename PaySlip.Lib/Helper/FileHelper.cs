﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace PaySlip.Lib.Helper
{
    public static class FileHelper
    {





        public static List<string> ReadPaySlip(string unprocessDirectory, string processDirecory, ILogger logger)
        {


            if (string.IsNullOrEmpty(unprocessDirectory))
                throw new ArgumentNullException("UnprocessDirectory cannot be null or empty", nameof(unprocessDirectory));

            if (string.IsNullOrEmpty(processDirecory))
                throw new ArgumentNullException("processDirecory cannot be null or empty", nameof(processDirecory));

            var paySlipDirectory = $"{processDirecory}\\payslips";
            CreateIfNotExists(paySlipDirectory);

            var backupDirectory = $"{processDirecory}\\backup";
            CreateIfNotExists(backupDirectory);

            var errorDirectory = $"{unprocessDirectory}\\error";
            CreateIfNotExists(errorDirectory);

            FileInfo fileInfo;
            List<string> payStipLines = new List<string>();

            string[] files = Directory.GetFiles(unprocessDirectory);

            if (files.Length > 0) fileInfo = new FileInfo(files[0]);
            else return payStipLines;



            int totalRows = 0;
            string employeeLine;
            var stopWatch = new Stopwatch(); //this is to logg performance diagnostic information


            var errorLoggerFile = $"{errorDirectory}\\line_error_{fileInfo.Name}";
            var paySlipFile = $"{paySlipDirectory}\\{DateTime.Now.Ticks}{fileInfo.Name}";

            // Read it line by line. 
            logger.LogInformation($"start Reading file {fileInfo.Name}");
            stopWatch.Start();
            using (var fileStreamReader = new StreamReader(fileInfo.FullName))
            {
                while ((employeeLine = fileStreamReader.ReadLine()) != null)
                {
                    try
                    {
                        totalRows++;
                        if (string.IsNullOrEmpty(employeeLine))
                            continue;
                        payStipLines.Add(employeeLine);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError($"file: {fileInfo.Name}, line: {employeeLine}, {ex.StackTrace}");
                    }
                }
            }
            logger.LogInformation($"End Reading file {fileInfo.Name}");
            var dateId = DateTime.Now.Ticks;
            fileInfo.MoveTo($"{backupDirectory}\\{dateId}_{fileInfo.Name}");
            logger.LogInformation($"File Moved to location: {backupDirectory}\\{dateId}_{fileInfo.Name}");
            return payStipLines;
        }

        public static void CreateIfNotExists(string directoryName)
        {
            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);
        }
    }











}

