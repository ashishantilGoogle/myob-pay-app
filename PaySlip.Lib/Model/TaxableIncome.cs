﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaySlip.Lib.Model
{
    public class TaxableIncome
    {

        public int AnnualTaxableIncome { get; set; }
        public int AnnualTaxFix { get; set; }
        public double PlusRate { get; set; }
    }
}
