using System;
using System.Collections.Generic;
using System.Text;

namespace PaySlip.Lib.Model
{
    public class EmployeeDetail
    {
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public int AnnualSalary { get; set; }
		public double SuperRate { get; set; }
		public string PaymentStartDate { get; set; }
        public bool IsValid { get; set; }
        public string ErrorMessage { get; set; }
    }
}
