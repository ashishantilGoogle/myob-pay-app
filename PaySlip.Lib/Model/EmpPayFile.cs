﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaySlip.Lib.Model
{
	public sealed class EmpPayFile
	{
		public string FileName { get; set; }
		public DateTime DateProcess { get; set; } = DateTime.Now;
	}
}
