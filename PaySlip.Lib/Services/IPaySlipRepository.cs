﻿using PaySlip.Lib.Model;
using System;
using System.Collections.Generic;

namespace PaySlip.Lib
{
    public interface IPaySlipRepository
    {

        List<EmployeeDetail> GetEmployees(string payDataFileDirectory, string payDataProcessedFileDirectory);

         void WriteSalery(string salery, string fileName);
    }
}   
