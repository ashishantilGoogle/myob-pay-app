﻿using PaySlip.Lib.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySlip.Lib.Services
{
    public interface IPaySlipCalculator
    {
        EmpPaySlip Process(EmployeeDetail employeeDetail);

    }
}
