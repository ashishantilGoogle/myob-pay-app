﻿using Microsoft.Extensions.Logging;
using PaySlip.Lib.Helper;
using PaySlip.Lib.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySlip.Lib.Services
{
    public class PaySlipProcessor: IPaySlipProcessor
    {
        private readonly IPaySlipRepository _payFileRepository;
        private readonly IPaySlipCalculator _paySlipCalculator;
        private readonly ILogger _logger;

        public PaySlipProcessor(IPaySlipCalculator paySlipCalculator, ILogger logger, IPaySlipRepository payFileRepository)
        {
            _logger = logger;
            
            _paySlipCalculator = paySlipCalculator;
            _payFileRepository = payFileRepository;

        }

        /// <summary>
        /// This is the main function 
        /// 1. Reads the payrol file. 
        /// 2. Process the records in the file 
        /// 3. Generate Pay slip in another file using tax/file repository
        /// </summary>
        /// <param name="unprocessDirectory"></param>
        /// <param name="processDirecory"></param>
        public void Process(string unprocessDirectory, string processDirecory)
        {
            if (string.IsNullOrEmpty(unprocessDirectory))
                throw new ArgumentNullException("UnprocessDirectory cannot be null or empty", nameof(unprocessDirectory));

            if (string.IsNullOrEmpty(processDirecory))
                throw new ArgumentNullException("processDirecory cannot be null or empty", nameof(processDirecory));

            var paySlipDirectory = $"{processDirecory}\\payslips";
            FileHelper.CreateIfNotExists(paySlipDirectory);

            var backupDirectory = $"{processDirecory}\\backup";
            FileHelper.CreateIfNotExists(backupDirectory);

            var errorDirectory = $"{unprocessDirectory}\\error";
            FileHelper.CreateIfNotExists(errorDirectory);

            
            var paySlipFile = $"{paySlipDirectory}\\{DateTime.Now.Ticks}";


            var errorLoggerFile = $"{errorDirectory}\\line_error_{DateTime.Now.Ticks}";

            var employees = _payFileRepository.GetEmployees(unprocessDirectory, processDirecory);

            if (employees.Count == 0) return; // no File processed 

            List<EmpPaySlip> paySlips = new List<EmpPaySlip>();
            List<string> paySlipsFail = new List<string>();

            // check if the employee records are valid.
            employees.ForEach(em => {
                if(em.IsValid)
                {
                    paySlips.Add(_paySlipCalculator.Process(em));
                }
                else
                {
                    paySlipsFail.Add($"Error While Processing Pay slip for {em.FirstName}{em.LastName}");
                }
               
            });

            _logger.LogInformation($"{paySlips.Count} records processed successfully out of {employees.Count}");

            paySlips.ForEach(ps =>
            {
                var psRow = $"{ps.Name},{ps.PayPeriod},{ps.GrossIncome},{ps.IncomeTax},{ps.NetIncome},{ps.Super}"; 
                _payFileRepository.WriteSalery(psRow, paySlipFile);

            });

            _logger.LogInformation($"{paySlips.Count} records written to paySlipFile");

            paySlipsFail.ForEach(errors=>
            {
                
                _payFileRepository.WriteSalery(errors, errorLoggerFile);

            });
        }       
    }
}
