﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaySlip.Lib.Services
{
    public interface IPaySlipProcessor
    {
        void Process(string unprocessDirectory, string processDirecory);
    }
}
