﻿using PaySlip.Lib.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySlip.Lib
{
    public interface ITaxCalculator
    {
        int Calculate(int annualSalary);

        TaxableIncome GetTaxRateRecord(int annualSalary);
    }
}
