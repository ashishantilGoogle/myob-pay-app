﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PaySlip.Lib.Helper;
using PaySlip.Lib.Model;

namespace PaySlip.Lib.Services
{
    /// <summary>
    /// This Class will return tax record and calculate month tax 
    /// </summary>
    public class TaxCalculator : ITaxCalculator
    {
        /// <summary>
        /// Gets the monthly Tax based on provided annual salary.
        /// </summary>
        /// <param name="annualSalary"></param>
        /// <returns></returns>
        public int Calculate(int annualSalary)
        {
            var taxRateRecord = GetTaxRateRecord(annualSalary);
            //under 18,000 nil
            if (taxRateRecord == null)
                return 0;

            //income tax = (3,572 + (60,050 - 37,000) x 0.325) / 12 = 921.9375 (round up) = 922
            var IncomeTax = (taxRateRecord.AnnualTaxFix + ((annualSalary - taxRateRecord.AnnualTaxableIncome) * taxRateRecord.PlusRate)) / 12;

            return (int)Math.Round((IncomeTax), MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Get the Tax slab based on annual salary
        /// </summary>
        /// <param name="annualSalary"></param>
        /// <returns></returns>
        public TaxableIncome GetTaxRateRecord(int annualSalary)
        {
            //tax table is hardcoded List
            return TaxableIncomeRates.GetRates()
                .Where(taxableIncome => taxableIncome.AnnualTaxableIncome < annualSalary)
                .OrderByDescending(taxableIncome => taxableIncome.AnnualTaxableIncome)
                .FirstOrDefault();
        }
    }
}
