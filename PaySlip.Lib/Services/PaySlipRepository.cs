﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PaySlip.Lib.Helper;
using PaySlip.Lib.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace PaySlip.Lib.Services
{
    public class PaySlipRepository : IPaySlipRepository
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configurationr;
        

        public PaySlipRepository(ILogger logger, IConfiguration configuration)
        {
            _logger = logger;
            _configurationr = configuration;
            
        }


        /// <summary>
        /// This function will retrieve from list of Employees record from Repository.
        /// </summary>
        /// <returns></returns>
        public List<EmployeeDetail> GetEmployees(string payDataFileDirectory, string payDataProcessedFileDirectory)
        {
            List<EmployeeDetail> employeeDetails = new List<EmployeeDetail>();
            try
            {
                var payslips = FileHelper.ReadPaySlip(payDataFileDirectory, payDataProcessedFileDirectory, _logger);

                if (payslips.Count == 0) return employeeDetails; // no record found 
                _logger.LogInformation($"Number of records found {payslips.Count}");

                payslips.ForEach(
                    epRecord =>
                    {
                        var employee = EmployeeHelper.GetEmployeeDetail(epRecord);
                        employeeDetails.Add(employee);
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetEmployees >> {ex.StackTrace}");
            }
            return employeeDetails;
        }
        
        private void CreateIfNotExists(string directoryName)
        {
            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);
        }

        public void WriteSalery(string salery, string fileName)
        {
            using (StreamWriter writer = File.AppendText(fileName))
            {
                writer.WriteLine(salery);
            }
        }
    }
}
