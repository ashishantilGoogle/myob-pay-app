﻿using Microsoft.Extensions.Logging;
using PaySlip.Lib;
using PaySlip.Lib.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySlip.Lib.Services
{
    public class PaySlipCalculator : IPaySlipCalculator
    {
        private readonly ILogger _logger;
        private readonly ITaxCalculator _taxCalculator;

        public PaySlipCalculator(ILogger logger,
            ITaxCalculator taxCalculator)
        {
            _logger = logger;
            _taxCalculator = taxCalculator;
        }

        /// <summary>
        /// Calculates Employee's pay slip 
        /// </summary>
        /// <param name="employeeDetail"></param>
        /// <returns></returns>
        public EmpPaySlip Process(EmployeeDetail employeeDetail)
        {
            var paySlip = new EmpPaySlip()
            {
                Name = $"{employeeDetail.FirstName} {employeeDetail.LastName}",
                PayPeriod = employeeDetail.PaymentStartDate
            };

            var monthlySalary = (double)employeeDetail.AnnualSalary / 12;
            paySlip.GrossIncome = (int)Math.Round((monthlySalary), MidpointRounding.AwayFromZero);

            paySlip.IncomeTax = _taxCalculator.Calculate(employeeDetail.AnnualSalary);
            paySlip.NetIncome = paySlip.GrossIncome - paySlip.IncomeTax;

            var monthlySuper = paySlip.GrossIncome * employeeDetail.SuperRate;
            paySlip.Super = (int)Math.Round((monthlySuper), MidpointRounding.AwayFromZero);

            return paySlip;
        }
    }
}
