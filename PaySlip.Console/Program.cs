﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PaySlip.Lib;
using PaySlip.Lib.Services;
using System;
using System.IO;
using System.Threading;

namespace PaySlip.Worker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting payApp worker!!");

            //use DI container to inject dependancies
            var services = new ServiceCollection();
            ConfigureServices(services);

            Console.WriteLine("DI container is ready!");
            //now DI container is ready to go
            var provider = services.BuildServiceProvider();
            var payslipApp = provider.GetService<PaySlipApp>();
            while (true)
            {
                payslipApp.Run();
                // wait for two sec to process next file
                Thread.Sleep(2000);
            }
            

            Console.ReadLine();
        }

        /// <summary>
        /// Add Configuration and other dependency
        /// </summary>
        /// <param name="services"></param>
        private static void ConfigureServices(IServiceCollection services)
        {
            Console.WriteLine("loading configuration!");
            //add configuration file
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            //build the configuration
            var configuration = builder.Build();
            Console.WriteLine("configuration loaded!");

            //buid the service ConfigureServices

            services.AddSingleton<IConfiguration>(configuration);
            services.AddSingleton<ILogger>(new LoggerFactory()
                .AddConsole()
                .CreateLogger<PaySlipApp>());
            services.AddLogging();

            //Resolve dependencey 
            services.AddTransient<IPaySlipCalculator, PaySlipCalculator>();
            services.AddTransient<IPaySlipProcessor, PaySlipProcessor>();
            services.AddTransient<IPaySlipRepository, PaySlipRepository>();
            services.AddTransient<ITaxCalculator, TaxCalculator>();
            
            services.AddTransient<PaySlipApp>();
        }
    }
}
