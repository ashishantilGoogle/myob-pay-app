﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PaySlip.Lib.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySlip.Worker
{
    class PaySlipApp
    {

        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IPaySlipProcessor _paySlipProcessor;

        public PaySlipApp(IConfiguration configuration, ILogger logger, IPaySlipProcessor paySlipProcessor)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _paySlipProcessor = paySlipProcessor;
            _logger.LogInformation($"Started employee pay file processing");
        }

        public void Run()
        {
            string employPayDataDirectory = _configuration["EmployPayDataDirectory"];
            string employeSalDataDirectory = _configuration["EmployeSalDataDirectory"];

            int allowedFileSizeInMB;
            int.TryParse(_configuration["AllowedFileSizeInMB"], out allowedFileSizeInMB);

            

            _paySlipProcessor.Process(employPayDataDirectory, employeSalDataDirectory);

        }
    }
}


