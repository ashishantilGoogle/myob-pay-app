﻿using Microsoft.Extensions.Logging;
using Moq;
using PaySlip.Lib.Helper;
using PaySlip.Lib.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Test.PaySlip.Lib.MocData;
using Xunit;

namespace PaySlip.Lib.Test
{
    public class PaySlipCalculatorTest
    {

        private readonly IPaySlipProcessor _paySlipProcess;
        private readonly Mock<ILogger> _logger;
        private readonly Mock<IPaySlipRepository> _iPaySlipRepository;
        private readonly Mock<ITaxCalculator> _taxCalculator;

        public PaySlipCalculatorTest()
        {
            _logger = new Mock<ILogger>();
            _iPaySlipRepository = new Mock<IPaySlipRepository>();
            _taxCalculator = new Mock<ITaxCalculator>();
        }



        [Theory]
        [Trait("Category", "TaxCalculatorService")]
        [MemberData(nameof(PaySlipTestData.EmployeePaySlipTestData),MemberType = typeof(PaySlipTestData))]
        public void Should_Return_Valid_Tax(string lineValue
            , string expectedName
            , int expectedGrossIncome
            , int expectedIncomeTax
            , int expectedNetIncome
            , int expectedSuper
            , string expectedPayPeriod)
        {
            //arrange
            var taxCalculator = new TaxCalculator();
            var paySlipCalculator = new PaySlipCalculator(_logger.Object, taxCalculator);

            var employee = EmployeeHelper.GetEmployeeDetail(lineValue);

            //act
            var result = paySlipCalculator.Process(employee);
            //assert
            Assert.Equal(expectedIncomeTax, result.IncomeTax);
            Assert.Equal(expectedGrossIncome, result.GrossIncome);
            Assert.Equal(expectedNetIncome, result.NetIncome);
            Assert.Equal(expectedSuper, result.Super);
            Assert.Equal(expectedPayPeriod, result.PayPeriod);
        }
    }
}
