﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.PaySlip.Lib.MocData
{
    public static class PaySlipTestData
    {
        /// <summary>
        /// Test data to perform tests
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<object[]> EmployeePaySlipTestData =>
        new List<object[]>
        {
             new object[] { "David,Rudd,60050,9%,01 March-31 March", "David Rudd", 5004, 922,4082,450,"01 March-31 March"},
             new object[] { "Ryan,Chen,120000,10%,01 March-31 March", "Ryan Chen", 10000, 2669, 7331, 1000, "01 March-31 March"},
             new object[] { "Ashish,Antil,130000,20%,01 March-31 March", "Ashish Antil",10833, 2978,7855,2167, "01 March-31 March"},
             new object[] { "Sonu,Singh,80000,5%,01 March-31 March", "Sonu Singh", 6667, 1462,5205,333, "01 March-31 March"}
        };
    }
}
