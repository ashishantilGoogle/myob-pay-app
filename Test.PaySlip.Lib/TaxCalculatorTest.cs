using PaySlip.Lib.Services;
using Xunit;

namespace Test.PayApp.Core
{
    public class TaxCalculatorTest
    {
        [Theory]
        [Trait("Category", "TaxCalculatorService")]
        [InlineData(23000, 0, 0.19)]
        [InlineData(50000, 3572, 0.325)]
        [InlineData(95000, 19822, 0.37)]
        [InlineData(185000, 54232, 0.45)]
        public void Should_Return_Valid_TaxRate(int annualSalary, int expectedTaxFix, double expectedPlusRate)
        {
            //arrange
            var taxCalculator = new TaxCalculator();
            //act
            var result = taxCalculator.GetTaxRateRecord(annualSalary);
            //assert
            Assert.Equal(expectedTaxFix, result.AnnualTaxFix);
            Assert.Equal(expectedPlusRate, result.PlusRate);
        }

        [Theory]
        [Trait("Category", "TaxCalculatorService")]
        [InlineData(12000)]
        [InlineData(5000)]
        [InlineData(18200)]
        public void ShouldReturnNoTaxRate(int annualSalary)
        {
            //arrange
            var taxCalculator = new TaxCalculator();
            //act
            var result = taxCalculator.GetTaxRateRecord(annualSalary);
            //assert
            Assert.Null(result);
        }

        [Theory]
        [Trait("Category", "TaxCalculatorService")]
        [InlineData(37000, 298)]
        [InlineData(60050, 922)]
        [InlineData(100000, 2053)]
        [InlineData(200000, 5269)]
        public void ShouldCalculateIncomeTax(int annualSalary, int expectedTax)
        {
            //arrange
            var taxCalculator = new TaxCalculator();
            //act
            var result = taxCalculator.Calculate(annualSalary);
            //assert
            Assert.Equal(expectedTax, result);
        }
    }
}
