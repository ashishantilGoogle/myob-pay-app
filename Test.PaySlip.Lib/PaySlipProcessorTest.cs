﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Internal;
using Moq;
using PaySlip.Lib.Model;
using PaySlip.Lib.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace PaySlip.Lib.Test
{
    public class PaySlipProcessorTest
    {

        private readonly IPaySlipProcessor _paySlipProcess;
        private readonly Mock<ILogger> _logger;
        private readonly Mock<IPaySlipRepository> _iPaySlipRepository;
        private readonly Mock<IPaySlipCalculator> _iPaySlipCalculator;

        public PaySlipProcessorTest()
        {
            _logger = new Mock<ILogger>();
            _iPaySlipRepository = new Mock<IPaySlipRepository>();
            _iPaySlipCalculator = new Mock<IPaySlipCalculator>();
            _iPaySlipCalculator = new Mock<IPaySlipCalculator>();
            _paySlipProcess = new PaySlipProcessor(_iPaySlipCalculator.Object, _logger.Object, _iPaySlipRepository.Object);
        }

        [Fact]
        [Trait("Category", "PaySlipProcessor")]
        public void Should_Throw_ArgumentNullException_If_FilePathIsEmpty()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                _paySlipProcess.Process(string.Empty, "c:/payslip/filepresnt.csv");
            });
        }

        [Fact]
        [Trait("Category", "PayProcessService")]
        public void Should_Throw_ArgumentNullException_If_PaySlipFullFileName_IsEmpty()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                _paySlipProcess.Process("c:/payslip/filepresnt.csv", "");
            });
        }
    }
}
